#!/bin/bash

#MIT License
#
#Copyright (c) 2021 cloudguan rcloudguan@163.com
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

OSNAME="Unknown"
ISN="Unknown"

function getLinuxDistribution() {
	#采用最简单和愚蠢的做法，不搞花活
	#最简单的判断，是否有os-release文件
	if type lsb_release > /dev/null 2>&1; then
		#是否有lsb-release文件
		OSNAME=$(lsb_release -si)
		ISN=apt-get
	elif [ -f /etc/os-release ]; then
		. /etc/os-release
		OSNAME=$NAME
		ISN=apt-get
	elif [ -f /etc/debian_version ]; then
		#是否是debian
		OSNAME=Deepin
		ISN=apt-get
	elif [ -f /etc/rehat_version ]; then
		#是否是redhat
		OSNAME=RedHat
		ISN=yum
	else
		echo "unkown linux distribution!"
	fi
}

#检查操作系统版本 如果是Unknown 就直接报错
getLinuxDistribution

if [[ $OSNAME == "Debian GNU/Linux" ]];then
    OSNAME="Debain"
    echo "osname set to debain"
fi

if [ $OSNAME = "Unknown" ];then
	echo "unkwon linux version!!"
	exit 1
fi 

#初始化安装protoc
if [ ! -f ../thirdparty/bin/protoc ]; then
    bash init_proto.sh
    if [ $? -gt 0 ];then
        echo "install protobuf error"
        exit 1
    fi
fi

#准备相关第三方库依赖
#python build_local_env_linux.py

dos2unix *

#chmod +x ./initgcc.sh
#chmod +x ./build_repo_linux.sh
#chmod +x ../tools/start_linux_box.sh
#chmod +x ../tools/start_linux_local_env.sh
#chmod +x ../tools/stop_linux_local_env.sh

dos2unix ./service-box-linux-dependency/zookeeper-bin/bin/*
chmod +x ./service-box-linux-dependency/zookeeper-bin/bin/zkServer.sh

cd service-box-linux-dependency

if [ ! -d ../../thirdparty/include/hiredis ];then
	mkdir -p ../../thirdparty/include/hiredis
fi

if [ ! -d ../../thirdparty/lib/linux ];then
	mkdir -p ../../thirdparty/lib/linux
fi

echo "copy thirdparty lib to framework!!"
cp hiredis/* ../../thirdparty/include/hiredis -rf
cp libcurl.a ../../thirdparty/lib/linux/libcurl.a -rf
cp libhashtable.a ../../thirdparty/lib/linux/libhashtable.a -rf
cp libhiredis.a ../../thirdparty/lib/linux/libhiredis.a -rf
cp libz.a ../../thirdparty/lib/linux/libz.a  -rf
cp libzookeeper.a ../../thirdparty/lib/linux/libzookeeper.a -rf

#设置默认路径,如果用户没有输入的话
INSTALL_PATH='../framework'
if [ $1 ];then
INSTALL_PATH=$1
fi 

if [ -d ../../cmakeproj ];then
	rm -rf ../../cmakeproj/*
else
	mkdir ../../cmakeproj
fi 

cd ../../cmakeproj
echo "install path: ${INSTALL_PATH}"
cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ ../src
make -j8 && make install
