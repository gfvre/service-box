# kratos::service::LocalCommand

service box client发送命令的处理类, LocalCommand与client通过HTTP协议通信, service box client提供了如下功能:

![输入图片说明](https://images.gitee.com/uploads/images/2020/1125/174310_1581839c_467198.png "屏幕截图.png")

## service box client启动方式
需要目标service box的配置文件与box_client在同一目录内。

1. 使用配置文件内的监听地址

假设service box配置文件为config.cfg, 连接到config.cfg内配置的监听地址
```
./box_client -c config.cfg --show-sys-alloc
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/1125/174540_54fae435_467198.png "屏幕截图.png")

2. 指定远程地址

使用指定的ip:host用来连接到远程service box

```
./box_client -c config.cfg --show-sys-alloc --host=192.168.6.219:6889
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/1125/174540_54fae435_467198.png "屏幕截图.png")

## 代码设计

service box client与service box使用同一个代码框架，区别是: client会在命令行及配置文件解析完成后即退出启动流程, 其他service box模块都不会启动，执行完HTTP请求后即退出
