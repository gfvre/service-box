﻿#pragma once

#include "root/rpc_root.h"

namespace kratos {
namespace service {

class LogHistory;

class ScriptService {
public:
public:
  virtual ~ScriptService() {}
  /**
   * 启动, 根据服务UUID读取配置
   *
   * \param service_name 服务名
   * \param uuid 服务UUID
   * \param publisher_ptr rpc::Publisher
   * \return true或false
   */
  virtual auto start(const std::string &service_name, rpc::ServiceUUID uuid,
                     rpc::Publisher *publisher_ptr) -> bool = 0;
  /**
   * 停止
   */
  virtual auto stop() -> bool = 0;
  /**
   * 主循环
   */
  virtual auto update(std::time_t tick) -> void = 0;
  /**
   * 调用服务方法
   */
  virtual auto call(rpc::StubCallPtr stub_call) -> void = 0;
  /**
   * 热更新，以文件为单元
   *
   * \param file_path 需要加载的文件
   * \return true或false
   */
  virtual auto hotfix_file(const std::string &file_path) -> bool = 0;
  /**
   * 热更新, 热更新一个代码字符串
   *
   * \param chunk 一段代码
   * \return true或false
   */
  virtual auto hotfix_chunk(const std::string &chunk) -> bool = 0;
  /**
   * 上一次服务调用是否yield
   */
  virtual auto is_last_call_yield() -> bool = 0;
  /**
   * @brief 开启调试器
   * @param name 调试器名称
   * @return
   */
  virtual auto open_debugger(const std::string &name) -> void = 0;
  /**
   * @brief 关闭调试器
   * @return
   */
  virtual auto close_debugger() -> void = 0;
  /**
   * @brief 启用调试器
   * @return
   */
  virtual auto disable_debugger() -> void = 0;
  /**
   * @brief 禁用调试器
   * @return
   */
  virtual auto enable_debugger() -> void = 0;
  /**
   * @brief 重新加载所有脚本
   * @return true或false
   */
  virtual auto reload() -> bool = 0;
  /**
   * @brief 重新加载所有脚本，但保留断点
   * @return true或false
   */
  virtual auto restart() -> bool = 0;
  /**
   * @brief 获取协程信息
   * @return 协程信息
   */
  virtual auto get_thread_info() -> std::string = 0;
  /**
   * @brief 获取日志历史
   * @return LogHistory接口指针
   */
  virtual auto get_log_history() -> LogHistory * = 0;
};

} // namespace service
} // namespace kratos
