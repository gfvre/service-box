#include "udp_loop.hh"
#include "detail/box_alloc.hh"
#include "util/object_pool.hh"
#include "box/box_network.hh"
#include "box/box_network_event.hh"
#include <cstring>

kratos::loop::UdpLoop::UdpLoop(service::BoxNetwork *network) {
  network_ = network;
}

kratos::loop::UdpLoop::~UdpLoop() {}

auto kratos::loop::UdpLoop::start() -> bool {
  loop_ = kratos::make_shared_pool_ptr<network::UdpChannelLoop>();
  return true;
}

auto kratos::loop::UdpLoop::stop() -> bool { return true; }

auto kratos::loop::UdpLoop::worker_update() -> void {
  loop_->update(std::bind(&UdpLoop::on_accept, this, std::placeholders::_1,
                          std::placeholders::_2),
                std::bind(&UdpLoop::on_receive, this, std::placeholders::_1,
                          std::placeholders::_2, std::placeholders::_3));
}

auto kratos::loop::UdpLoop::do_worker_event(
    const service::NetEventData &event_data) -> void {
  if (!loop_) {
    return;
  }
  switch (event_data.event_id) {
  case service::NetEvent::listen_request: {
    do_listen_request(event_data);
  } break;
  case service::NetEvent::connect_request: {
    do_connect_request(event_data);
  } break;
  case service::NetEvent::send_data_notify: {
    do_send_request(event_data);
  } break;
  case service::NetEvent::close_request: {
    do_close_request(event_data);
  } break;
  default:
    break;
  }
}

auto kratos::loop::UdpLoop::get_network() -> service::BoxNetwork * {
  return network_;
}

auto kratos::loop::UdpLoop::do_listen_request(
    const service::NetEventData &event_data) -> void {
  auto channel_id = loop_->newAcceptor(
      event_data.listen_request.host, (std::uint16_t)event_data.listen_request.port,
      event_data.listen_request.name, NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE);
  service::NetEventData response{};
  response.event_id = service::NetEvent::listen_response;
  auto length =
      static_cast<int>(std::strlen(event_data.listen_request.name) + 1);
  response.listen_response.name = service::box_malloc(length);
  memcpy(response.listen_response.name, event_data.listen_request.name, length);
  if (!channel_id) {
    response.listen_response.channel_id = 0;
    response.listen_response.success = false;
  } else {
    response.listen_response.channel_id = channel_id;
    response.listen_response.success = true;
  }
  if (!get_network()->get_net_queue().send(response)) {
    // 发送失败，清理资源
    // 发生这种情况，会导致逻辑线程无法得知监听器是否启动成功
    response.clear();
    loop_->close(channel_id);
  } else {
    get_network()->get_listener_name_map()[channel_id] = {
        channel_id, event_data.listen_request.name};
  }
}

auto kratos::loop::UdpLoop::do_connect_request(
    const service::NetEventData &event_data) -> void {
  auto channel_id = loop_->newConnector(
      event_data.connect_request.host, (std::uint16_t)event_data.connect_request.port,
      NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE);
  service::NetEventData response{};
  response.event_id = service::NetEvent::connect_response;
  auto length = std::strlen(event_data.connect_request.name) + 1;
  response.connect_response.name = service::box_malloc(length);
  memset(response.connect_response.name, 0, length);
  memcpy(response.connect_response.name, event_data.connect_request.name,
         length);
  if (!channel_id) {
    response.connect_response.channel_id = 0;
    response.connect_response.success = false;
  } else {
    response.connect_response.channel_id = channel_id;
    response.connect_response.success = true;
  }
  if (!get_network()->get_net_queue().send(response)) {
    // 发送失败，清理资源
    response.clear();
    loop_->close(channel_id);
  }
}

auto kratos::loop::UdpLoop::do_send_request(
    const service::NetEventData &event_data) -> void {
  if (!loop_->exists(event_data.send_data_notify.channel_id)) {
    return;
  }
  loop_->send(event_data.send_data_notify.channel_id,
              event_data.send_data_notify.data_ptr,
              event_data.send_data_notify.length);
}

auto kratos::loop::UdpLoop::do_close_request(
    const service::NetEventData &event_data) -> void {
  loop_->close(event_data.close_request.channel_id);
  // 发送到逻辑线程
  service::NetEventData notify_data{};
  notify_data.event_id = service::NetEvent::close_notify;
  notify_data.close_notify.channel_id = event_data.close_request.channel_id;
  if (!get_network()->get_net_queue().send(notify_data)) {
    // 发送失败，清理资源
    notify_data.clear();
  }
}

auto kratos::loop::UdpLoop::on_accept(std::uint64_t channel_id,
                                      const std::string &name) -> void {
  // 发送到逻辑线程
  service::NetEventData event_data{};
  event_data.event_id = service::NetEvent::accept_notify;
  event_data.accept_notify.channel_id = channel_id;
  event_data.accept_notify.name = service::box_malloc(name.size() + 1);
  memcpy(event_data.accept_notify.name, name.c_str(), name.size() + 1);
  if (!get_network()->get_net_queue().send(event_data)) {
    // 内部错误或发送失败, 销毁资源
    loop_->close(channel_id);
    event_data.clear();
  }
}

auto kratos::loop::UdpLoop::on_receive(std::uint64_t channel_id,
                                       const char *data, std::uint32_t size)
    -> void {
  service::NetEventData event_data{};
  event_data.event_id = service::NetEvent::recv_data_notify;
  event_data.recv_data_notify.channel_id = channel_id;
  event_data.recv_data_notify.data_ptr = service::box_malloc(size);
  event_data.recv_data_notify.length = size;
  memcpy(event_data.recv_data_notify.data_ptr, data, size);
  // 发送到逻辑线程
  if (!get_network()->get_net_queue().send(event_data)) {
    event_data.clear();
    // 发送失败，清理资源
    loop_->close(channel_id);
  }
}
