﻿#pragma once

#include <functional>
#include <memory>
#include <string>
#include <ctime>

namespace kratos {
namespace service {

// 服务注册接口
class ServiceRegister {
public:
  virtual ~ServiceRegister() {}
  // 启动并连接服务器
  // @param servers 多个服务地址, 例如:"127.0.0.1:2011,127.0.0.1:2012"
  // @param timeout 连接超时时间（毫秒
  // @param version 版本号
  // @retval true 成功
  // @retval false 失败
  virtual bool start(const std::string &servers, int timeout, const std::string& version) = 0;
  // 主循环
  // @param tick 当前时间戳，毫秒
  virtual void update(std::time_t tick) = 0;
  // 与服务器断开连接
  // @retval true 成功
  // @retval false 失败
  virtual bool stop() = 0;
  // 向服务器注册服务
  // @param name 服务名,例如:"/type/dns/"
  // @param host 用于连接本服务的地址
  // @retval true 成功
  // @retval false 失败
  virtual bool register_service(const std::string &name,
                                const std::string &host) = 0;
  // 取消服务注册
  // @param name 服务名,例如:"/type/dns/"
  // @param host 用于连接本服务的地址
  // @retval true 成功
  // @retval false 失败
  virtual bool unregister_service(const std::string &name,
                                  const std::string &host) = 0;
};

// 根据服务器类型创建注册器
// @param type 服务器类型, 支持："zookeeper"
// @return 注册器实例
extern std::shared_ptr<ServiceRegister> getRegister(const std::string &type);

} // namespace service
} // namespace kratos
