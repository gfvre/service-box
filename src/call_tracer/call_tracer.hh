#pragma once

#include <cstdint>
#include <ctime>
#include <string>

namespace kratos {
namespace config {
class BoxConfig;
}
} // namespace kratos

namespace kratos {
namespace service {

/**
 * 调用链端类型.
 */
enum class TraceType {
  NONE,
  CLIENT = 1, ///< 客户端
  SERVER,     ///< 服务器
  PRODUCER,   ///< 生产者
  CONSUMER,   ///< 消费者
};

/**
 * 调用链打点类型.
 */
enum class TraceDetailType {
  NONE,
  PROXY_CALL_REMOTE_SERVICE = 1, ///< 代理调用远程服务
  PROXY_RECV_SERVICE_RESULT,     ///< 代理接收到远程服务返回
  SERVICE_RECV_PROXY_REQUEST,    ///< 服务接收到代理调用请求
  SERVICE_SEND_PROXY_RESULT,     ///< 服务发送代理调用结果
};

/**
 * 调用链信息.
 */
struct TraceInfo {
  TraceDetailType detail_type{TraceDetailType::NONE}; ///< 详细信息
  TraceType type{TraceType::NONE};                    ///< 类型
  std::string trace_id;                               ///< 跟踪ID
  std::uint64_t span_id{0};                           ///< 阶段ID
  std::uint64_t parent_span_id{0};                    ///< 父阶段ID
  std::string service_name;                           ///< 服务名
  std::string method_name;                            ///< 方法名
  std::string from_ip;                                ///< 发起方IP
  std::int32_t from_port{0};                          ///< 发起方端口
  std::string target_ip;                              ///< 目标方IP
  std::int32_t target_port{0};                        ///< 目标方端口
  std::int32_t pid{0};                                ///< 发生所在进程PID
  std::time_t timestamp_ms{0}; ///< 发生时的时间戳，微秒
  std::string param_info;      ///< 携带的参数
  std::time_t duration{0};     ///< 持续时间，微秒
};

/**
 * 调用链记录.
 */
class CallTracer {
public:
  virtual ~CallTracer() {}
  /**
   * 启动.
   *
   * \param config 配置引用
   * \return true成功, false失败
   */
  virtual auto start(kratos::config::BoxConfig &config) -> bool = 0;
  /**
   * 记录一次链路信息.
   *
   * \param trace_info 调用信息
   * \return
   */
  virtual auto trace(const TraceInfo &trace_info) -> void = 0;
  /**
   * 主循环.
   *
   * \return
   */
  virtual auto update(std::time_t now) -> void = 0;
};

} // namespace service
} // namespace kratos
