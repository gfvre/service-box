#include "method_type.hh"

#include "klogger/interface/logger.h"
#include "json/json.h"
#include "util/os_util.hh"
#include "util/singleton.hh"
#include "util/string_util.hh"
#include "box/box_network.hh"

#include <google/protobuf/compiler/importer.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/message.h>
#include <google/protobuf/reflection.h>

#include <filesystem>

namespace kratos {
namespace util {

MsgFactory::MsgFactory(service::BoxNetwork *network) {
  network_ = network;
  factory_ = make_unique_pool_ptr<ProtobufDynamicMessageFactory>();
}

MsgFactory::~MsgFactory() {
  msg_factory_map_.clear();
  factory_.reset();
  importer_.reset();
}

MethodType::MethodType(const ProtobufDescriptor *request_descriptor,
                       const ProtobufDescriptor *response_descriptor,
                       bool is_oneway, const std::string &service_name,
                       const std::string &service_method_name, int call_timeout,
                       ProtobufMessage *request, ProtobufMessage *response,
                       const std::string &uuid, bool has_ret_value) noexcept
    : request_message_descriptor(request_descriptor),
      response_message_descriptor(response_descriptor), oneway(is_oneway),
      service_name(service_name), method_name(service_method_name),
      timeout(call_timeout), request_message(request),
      response_message(response), uuid_string(uuid),
      has_ret_value(has_ret_value) {
  lua_real_method_name = "_" + uuid + "_" + method_name;
}

MethodType::MethodType(const MethodType &rht) noexcept
    : request_message_descriptor(rht.request_message_descriptor),
      response_message_descriptor(rht.response_message_descriptor),
      oneway(rht.oneway), service_name(rht.service_name),
      method_name(rht.method_name), timeout(rht.timeout),
      request_message(rht.request_message),
      response_message(rht.response_message), uuid_string(rht.uuid_string),
      lua_real_method_name(rht.lua_real_method_name),
      has_ret_value(rht.has_ret_value) {}

MethodType::MethodType(MethodType &&rht) noexcept
    : request_message_descriptor(rht.request_message_descriptor),
      response_message_descriptor(rht.response_message_descriptor),
      oneway(rht.oneway), service_name(std::move(service_name)),
      method_name(std::move(rht.method_name)), timeout(rht.timeout),
      request_message(std::move(rht.request_message)),
      response_message(std::move(rht.response_message)),
      uuid_string(std::move(rht.uuid_string)),
      lua_real_method_name(std::move(rht.lua_real_method_name)),
      has_ret_value(rht.has_ret_value) {}

bool MethodType::has_retval() const {
  return (!oneway && (response_message != nullptr));
}

auto MsgFactory::load(const std::string &idl_json_file,
                      const std::string &idl_proto_root_dir,
                      const std::string &file_name) -> bool {
  Json::Value root;
  std::string error;
  //
  // 获取JSON对象
  //
  auto ret = util::get_json_root_from_file(idl_json_file, root, error);
  if (!ret) {
    if (network_) {
      std::string json_error;
      json_error = "Parse JSON file failed: " + idl_json_file;
      network_->get_logger_appender()->write(klogger::Logger::FATAL,
                                             json_error.c_str());
    }
    return false;
  }
  ProtobufDiskSourceTree source_tree;
  //
  // 设置source tree的路径
  //
  source_tree.MapPath("proto", idl_proto_root_dir);
  if (!importer_) {
    importer_ = make_unique_pool_ptr<ProtobufImporter>(&source_tree, nullptr);
  }
  //
  // 导入文件描述
  //
  const auto *file_descriptor = importer_->Import("proto/" + file_name);
  if (!file_descriptor) {
    if (network_) {
      std::string error_str;
      error_str = "Cannot import PROTO file[" + file_name + "]";
      network_->get_logger_appender()->write(klogger::Logger::FATAL,
                                             error_str.c_str());
    }
    return false;
  }
  return load(root, file_descriptor);
}

auto MsgFactory::load(const std::string &idl_json_root_dir,
                      const std::string &idl_proto_root_dir) -> bool {
  //
  // 获取所有的JSON文件
  //
  std::vector<std::string> proto_file_names;
  if (!util::get_file_in_directory(idl_proto_root_dir, ".proto",
                                   proto_file_names)) {
    if (network_) {
      std::string error;
      error = "Retrive .proto file failed in[" + idl_proto_root_dir + "]";
      network_->get_logger_appender()->write(klogger::Logger::FATAL,
                                             error.c_str());
    }
    return false;
  }
  ProtobufDiskSourceTree source_tree;
  //
  // 设置source tree的路径
  //
  source_tree.MapPath("proto", idl_proto_root_dir);
  if (!importer_) {
    importer_ = make_unique_pool_ptr<ProtobufImporter>(&source_tree, nullptr);
  }
  for (const auto &file_name : proto_file_names) {
    //
    // 没有扩展名的文件名
    //
    auto file_name_no_ext =
        std::filesystem::path(file_name).stem().stem().string();
    //
    // source tree虚拟文件路径
    //
    auto virtual_proto_file_path =
        "proto/" + std::filesystem::path(file_name).filename().string();
    const auto *descriptor = importer_->Import(virtual_proto_file_path);
    if (!descriptor) {
      if (network_) {
        std::string error_str;
        error_str = "Cannot import PROTO file[" + file_name + "]";
        network_->get_logger_appender()->write(klogger::Logger::FATAL,
                                               error_str.c_str());
      }
      return false;
    }
    if (network_) {
      network_->get_logger_appender()->write(
          klogger::Logger::INFORMATION,
          ("[MsgFactory]Import PROTO file[" + file_name + "]").c_str());
    }
    std::string json_file_path = util::complete_path(
        idl_json_root_dir, file_name_no_ext + ".idl.protobuf.json");
    Json::Value root;
    std::string error;
    auto ret = util::get_json_root_from_file(json_file_path, root, error);
    if (!ret) {
      if (network_) {
        std::string json_error;
        json_error = "Parse JSON file failed[" + json_file_path + "]";
        network_->get_logger_appender()->write(klogger::Logger::FATAL,
                                               json_error.c_str());
      }
      return false;
    } else {
      if (network_) {
        network_->get_logger_appender()->write(
            klogger::Logger::INFORMATION,
            ("[MsgFactory]Import JSON file[" + json_file_path + "]").c_str());
      }
    }
    if (!load(root, descriptor)) {
      if (network_) {
        std::string json_error;
        json_error = "Parse protobuf file failed[" + file_name + "]";
        network_->get_logger_appender()->write(klogger::Logger::FATAL,
                                               json_error.c_str());
      }
      return false;
    }
  }
  return true;
}

auto MsgFactory::load(const Json::Value &idl_json_root,
                      const ProtobufFileDescriptor *file_descriptor) -> bool {
  if (!file_descriptor) {
    return false;
  }
  for (const auto &service : idl_json_root["services"]) {
    //
    // 校验
    //
    if (!service.isMember("name") || !service.isMember("methods")) {
      if (network_) {
        std::string json_error;
        json_error =
            "JSON format invalid, service need 'name' and 'methods' attribute";
        network_->get_logger_appender()->write(klogger::Logger::FATAL,
                                               json_error.c_str());
      }
    }
    auto service_name = service["name"].asString();
    for (const auto &method : service["methods"]) {
      //
      // PB内message的名字, 对应方法的参数, {service name}_{method name}_args
      //
      auto arg_name = file_descriptor->package() + "." +
                      service["name"].asString() + "_" +
                      method["name"].asString() + "_args";
      //
      // PB内message的名字, 对应方法的返回值, {service name}_{method name}_ret
      //
      auto ret_name = file_descriptor->package() + "." +
                      service["name"].asString() + "_" +
                      method["name"].asString() + "_ret";
      const auto *req_desc = importer_->pool()->FindMessageTypeByName(arg_name);
      const auto *ack_desc = importer_->pool()->FindMessageTypeByName(ret_name);
      auto oneway = method.isMember("oneway");
      //
      // 如果返回值不是void类型, 则认为是有返回值的
      //
      auto has_ret_value = (method["retType"]["IdlType"].asString() != "void");
      ProtobufMessage *arg_msg = nullptr;
      ProtobufMessage *ret_msg = nullptr;
      //
      // 方法的参数和返回值对象只建立一个
      //
      if (req_desc) {
        const auto *arg_type = factory_->GetPrototype(req_desc);
        if (arg_type) {
          arg_msg = arg_type->New();
        }
      }
      if (ack_desc) {
        const auto *ret_type = factory_->GetPrototype(ack_desc);
        if (ret_type) {
          ret_msg = ret_type->New();
        }
      }
      auto method_name = method["name"].asString();
      auto timeout = method["timeout"].asInt();
      auto service_uuid_str = service["uuid"].asString();
      auto service_uuid = std::stoull(service_uuid_str);
      MethodType method_call{
          req_desc, ack_desc, oneway,  service_name,     method_name,
          timeout,  arg_msg,  ret_msg, service_uuid_str, has_ret_value};
      //
      // 放入消息表, 后续用于快速获取
      //
      msg_factory_map_[service_uuid].emplace_back(std::move(method_call));
      method_table_[service_name + "::" + method_name] = {
          std::stoull(service_uuid_str), method["index"].asUInt()};
    }
  }
  return true;
}

auto MsgFactory::new_call_param(rpc::ServiceUUID service_uuid,
                                rpc::MethodID method_id) noexcept
    -> ProtobufMessage * {
  auto it = msg_factory_map_.find(service_uuid);
  if (it == msg_factory_map_.end()) {
    return nullptr;
  }
  const auto &methods = it->second;
  if ((method_id > methods.size()) || (method_id == 0)) {
    return nullptr;
  }
  auto &req_msg_ptr = methods[method_id - 1].request_message;
  if (!req_msg_ptr) {
    return nullptr;
  }
  //
  // 清理后返回, 复用
  //
  req_msg_ptr->Clear();
  return req_msg_ptr.get();
}

auto MsgFactory::new_call_return(rpc::ServiceUUID service_uuid,
                                 rpc::MethodID method_id) noexcept
    -> ProtobufMessage * {
  auto it = msg_factory_map_.find(service_uuid);
  if (it == msg_factory_map_.end()) {
    return nullptr;
  }
  const auto &methods = it->second;
  if ((method_id > methods.size()) || (method_id == 0)) {
    return nullptr;
  }
  auto &rep_msg_ptr = methods[method_id - 1].response_message;
  if (!rep_msg_ptr) {
    return nullptr;
  }
  //
  // 清理后返回, 复用
  //
  rep_msg_ptr->Clear();
  return rep_msg_ptr.get();
}

auto MsgFactory::new_call_param(const std::string &service_name,
                                const std::string &method_name) noexcept
    -> ProtobufMessage * {
  auto name = service_name + "::" + method_name;
  auto it = method_table_.find(name);
  if (it == method_table_.end()) {
    return nullptr;
  }
  return new_call_param(it->second.first, it->second.second);
}

auto MsgFactory::new_call_return(const std::string &service_name,
                                 const std::string &method_name) noexcept
    -> ProtobufMessage * {
  auto name = service_name + "::" + method_name;
  auto it = method_table_.find(name);
  if (it == method_table_.end()) {
    return nullptr;
  }
  return new_call_return(it->second.first, it->second.second);
}

auto MsgFactory::get_method_info(const std::string &service_name,
                                 const std::string &method_name)
    -> std::pair<rpc::ServiceUUID, rpc::MethodID> {
  auto name = service_name + "::" + method_name;
  auto it = method_table_.find(name);
  if (it == method_table_.end()) {
    return {0, 0};
  }
  return {it->second.first, it->second.second};
}

auto MsgFactory::get_type(rpc::ServiceUUID service_uuid,
                          rpc::MethodID method_id) const noexcept
    -> const MethodType * {
  auto it = msg_factory_map_.find(service_uuid);
  if (it == msg_factory_map_.end()) {
    return nullptr;
  }
  const auto &methods = it->second;
  if ((method_id > methods.size()) || (method_id == 0)) {
    return nullptr;
  }
  return &methods[method_id - 1];
}

} // namespace util
} // namespace kratos
