﻿#pragma once

#include "util/lua/lua_export_class.h"

namespace kratos {
namespace service {
class ServiceBox;
}
} // namespace kratos

namespace kratos {
namespace time {
class LocalTimeImpl;
} // namespace time
} // namespace kratos

namespace kratos {
namespace lua {

class LuaServiceImpl;

/**
 * @brief Time类lua导出包装
 */
class LuaTime : public LuaExportClass {
  kratos::service::ServiceBox *box_{nullptr}; ///< 服务容器
  lua_State *L_{nullptr};                     ///< lua虚拟机
  kratos::unique_pool_ptr<kratos::time::LocalTimeImpl>
      times_ptr_;                    ///< Redis实例
  LuaServiceImpl *service_{nullptr}; ///< 服务容器

public:
  /**
   * @brief 构造
   * @param box 服务容器
   * @param L 虚拟机
   * @param service lua服务
   */
  LuaTime(kratos::service::ServiceBox *box, lua_State *L,
          LuaServiceImpl *service);
  /**
   * 析构
   */
  virtual ~LuaTime();
  virtual auto do_register() -> bool override;
  virtual auto update(std::time_t ms) -> void override;
  virtual auto do_cleanup() -> void override;

private:
  static int lua_get_millionsecond(lua_State *l);
  static int lua_get_second(lua_State *l);
  static int lua_utc_diff_second(lua_State *l);
  static int lua_diff_days_now(lua_State *l);
  static int lua_diff_days(lua_State *l);
  static int lua_get_date(lua_State *l);
  static int lua_date_from_string(lua_State *l);
  static int lua_data_from_time(lua_State *l);
  static int lua_is_same_day(lua_State *l);
  static int lua_is_same_week(lua_State *l);
  static int lua_is_same_month(lua_State *l);
};

} // namespace lua
} // namespace kratos
