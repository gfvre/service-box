#pragma once

#define PY_SSIZE_T_CLEAN
#include "Python.h"

#include "box/box_network.hh"
#include "detail/scheduler_impl.hh"
#include "knet.h"
#include "root/rpc_defines.h"
#include <ctime>
#include <memory>
#include <string>
#include <unordered_map>
#include <utility>

#include <iostream>

namespace google {
namespace protobuf {
class Reflection;
class Message;
class Descriptor;
class FieldDescriptor;
class FileDescriptor;
class DynamicMessageFactory;
class Arena;
namespace compiler {
class Importer;
class DiskSourceTree;
} // namespace compiler
} // namespace protobuf
} // namespace google

using ProtobufReflection = google::protobuf::Reflection;
using ProtobufMessage = google::protobuf::Message;
using ProtobufFieldDescriptor = google::protobuf::FieldDescriptor;
using ProtobufImporter = google::protobuf::compiler::Importer;
using ProtobufFileDescriptor = google::protobuf::FileDescriptor;
using ProtobufDescriptor = google::protobuf::Descriptor;
using ProtobufDynamicMessageFactory = google::protobuf::DynamicMessageFactory;
using ProtobufArena = google::protobuf::Arena;
using ProtobufDiskSourceTree = google::protobuf::compiler::DiskSourceTree;

namespace kratos {
namespace service {
class SchedulerImpl;
class ServiceRegister;
class ServiceFinder;
class ServiceLayer;
class RpcProbeImpl;
} // namespace service
namespace config {
class BoxConfigImpl;
}
} // namespace kratos

namespace kratos {
namespace util {

class CppRuntime;

/**
 * Google Protobuf coder/encoder
 */
class ProtobufCoder {
  CppRuntime *runtime_{nullptr};

public:
  /**
   * 构造
   *
   */
  ProtobufCoder(CppRuntime *runtime = nullptr);
  /**
   * 析构.
   *
   */
  ~ProtobufCoder();
  /**
   * 将google::protobuf::Message对象序列化为Python对象
   *
   * \param pb_msg google::protobuf::Message对象
   * \return Python对象
   */
  auto decode(const ProtobufMessage &pb_msg) -> PyObject *;
  /**
   * 将Python对象序列化到google::protobuf::Message对象.
   *
   * \param py_obj_ptr Python对象
   * \param pb_msg google::protobuf::Message对象
   * \return
   */
  auto encode(PyObject *py_obj_ptr, ProtobufMessage &pb_msg) -> void;
  /**
   * 写入错误日志.
   *
   * \param log 错误日志
   * \return
   */
  auto write_error_log(const std::string &log) -> void;

private:
  /**
   * google::protobuf::Message对象成员序列化为Python对象
   *
   * \param msg google::protobuf::Message对象
   * \param field google::protobuf::FieldDescriptor对象
   * \return Python对象
   */
  auto on_field(const ProtobufMessage &msg,
                const ProtobufFieldDescriptor *field) -> PyObject *;
  /**
   * google::protobuf::Message map对象成员序列化为Python对象
   *
   * \param msg google::protobuf::Message map对象
   * \param field google::protobuf::FieldDescriptor对象成员
   * \return Python对象
   */
  auto on_map_field(const ProtobufMessage &msg,
                    const ProtobufFieldDescriptor *field) -> PyObject *;
  /**
   * google::protobuf::Message map key对象序列化为Python对象
   *
   * \param msg google::protobuf::Message map对象
   * \param field google::protobuf::FieldDescriptor对象
   * \return Python对象
   */
  auto on_map_field_key(const ProtobufMessage &msg,
                        const ProtobufFieldDescriptor *field) -> PyObject *;
  /**
   * google::protobuf::Message map value对象序列化为Python对象
   *
   * \param msg google::protobuf::Message map对象
   * \param field google::protobuf::FieldDescriptor对象
   * \return Python对象
   */
  auto on_map_field_value(const ProtobufMessage &msg,
                          const ProtobufFieldDescriptor *field) -> PyObject *;
  /**
   * google::protobuf::Message repeated item对象序列化为Python对象
   *
   * \param msg google::protobuf::Message item对象
   * \param field google::protobuf::FieldDescriptor对象
   * \return Python对象
   */
  auto on_repeated_field(const ProtobufMessage &msg,
                         const ProtobufFieldDescriptor *field) -> PyObject *;

private:
  /**
   * 将google::protobuf::Message对象序列化为PyObject对象
   *
   * \param py_obj_ptr PyObject对象
   * \param msg google::protobuf::Message对象
   * \param ref google::protobuf::Reflection对象
   * \param field google::protobuf::FieldDescriptor对象
   * \return
   */
  auto on_object_value(PyObject *py_obj_ptr, ProtobufMessage &msg,
                       const ProtobufReflection *ref,
                       const ProtobufFieldDescriptor *field) -> void;
  /**
   * 将google::protobuf::Message map对象序列化为PyObject对象
   *
   * \param py_obj_ptr PyObject对象
   * \param msg google::protobuf::Message对象
   * \param ref google::protobuf::Reflection对象
   * \param field google::protobuf::FieldDescriptor对象
   * \return
   */
  auto on_object_map(PyObject *py_obj_ptr, ProtobufMessage &msg,
                     const ProtobufReflection *ref,
                     const ProtobufFieldDescriptor *field) -> void;
  /**
   * 将google::protobuf::Message repeated对象序列化为PyObject对象
   *
   * \param py_obj_ptr PyObject对象
   * \param msg google::protobuf::Message对象
   * \param ref google::protobuf::Reflection对象
   * \param field google::protobuf::FieldDescriptor对象
   * \return
   */
  auto on_object_repeated_value(PyObject *py_obj_ptr, ProtobufMessage &msg,
                                const ProtobufReflection *ref,
                                const ProtobufFieldDescriptor *field) -> void;
};

namespace KratosService = kratos::service;

/**
 * RPC框架
 *
 * RPC协议序列化/反序列化, 服务发现, 配置加载
 */
class CppRuntime : public kratos::service::BoxNetwork {
  ProtobufCoder coder_; ///< PyObject <-> google::protobuf::Message
  std::unique_ptr<char[]> char_buffer_; ///< 临时缓冲区
  std::size_t buffer_length_{0};        ///<  临时缓冲区长度
  /**
   * 调用信息, 用于记录调用与方法的对应关系
   */
  struct CallInfo {
    std::time_t deadline{0};       ///< 调用超时时间戳, 毫秒
    rpc::ServiceID service_id{0};  ///< 服务ID
    rpc::ServiceUUID uuid{0};      ///< 服务UUID
    rpc::MethodID method_id{0};    ///< 方法ID
    std::uint64_t timer_handle{0}; ///< 定时器ID
    operator bool() const { return ((uuid != 0) && (method_id != 0)); }
  };
  using CallInfoMap = std::unordered_map<rpc::CallID, CallInfo>;
  CallInfoMap call_info_map_; ///< 调用信息表
  std::unique_ptr<kratos::service::SchedulerImpl> sched_ptr_; ///< 定时器
  PyObject *py_log_cb_{nullptr}; ///< Python提供的日志回调函数
#ifndef PYTHON_SDK
  std::unique_ptr<KratosService::ServiceRegister>
      service_register_; ///< 服务注册
  std::unique_ptr<KratosService::ServiceFinder> service_finder_; ///< 服务发现
  std::unique_ptr<KratosService::ServiceLayer> service_layer_; ///< 服务缓存
  std::unique_ptr<kratos::service::RpcProbeImpl> probe_;       ///< 探针
#else
  kratos::service::BoxChannelPtr proxy_channel_; ///< 代理网关管道
  std::vector<std::string> proxy_hosts_;         ///< 网关地址列表
  std::time_t last_reconn_ms_{0};    ///< 上次尝试重连时间戳, 毫秒
  std::time_t reconn_timeout_{5000}; ///< 重连间隔
  bool is_connecting_{false};        ///< 是否正在连接
#endif                                                    // PYTHON_SDK
  std::unique_ptr<kratos::config::BoxConfigImpl> config_; ///< 配置
  using PackList = std::list<PyObject *>;
  PackList pack_list_; ///< 事件队列
  using StringMap = std::unordered_map<std::string, std::string>;
  StringMap registered_service_map_;             ///< 已经注册的服务
  service::SchedulerCallback timer_cb_func_;     ///< 定时器回调
  service::SchedulerCallback ext_timer_cb_func_; ///< 非RPC定时器回调
  // 订阅信息
  struct SubInfo {
    std::uint64_t channel_id{0};                        ///< 所属管道
    rpc::ServiceUUID uuid{rpc::INVALID_SERVICE_UUID};   ///< 服务UUID
    rpc::MethodID method_id{rpc::INVALID_METHOD_ID};    ///< 方法ID
    rpc::ServiceID service_id{rpc::INVALID_SERVICE_ID}; ///< 服务实例ID
    std::string evt_name;                               ///< 事件名
    rpc::ProxyID proxy_id{rpc::INVALID_PROXY_ID};       ///< 代理ID
  };
  using SubInfoMap = std::unordered_map<rpc::SubID, SubInfo>;
  SubInfoMap sub_info_map_; ///< {SubID, SubInfo}

public:
  /**
   * 构造
   */
  CppRuntime();
  /**
   * 析构
   */
  virtual ~CppRuntime();

  /**
   * 获取服务容器配置
   *
   * \return 服务容器配置
   */
  virtual auto get_config() -> kratos::config::BoxConfig & override;
  /**
   * 获取日志添加器.
   *
   * \return
   */
  virtual auto get_logger_appender() -> klogger::Appender * override;

  /**
   * 获取本地化实例.
   *
   * \return 本地化实例
   */
  virtual auto get_lang() -> kratos::lang::Lang * override;
  /**
   * 开启监听事件
   *
   * \param name 监听器名称
   * \param success 成功或失败
   * \param channel 监听管道
   */
  virtual void on_listen(const std::string & /*name*/, bool /*success*/,
                         kratos::service::BoxChannelPtr & /*channel*/) override;
  /**
   * 接受一个新的连接建立
   *
   * \param channel 新建立的管道
   */
  virtual void on_accept(kratos::service::BoxChannelPtr & /*channel*/) override;
  /**
   * 连接到一个远程监听器
   *
   * \param name 连接器名称
   * \param success 成功或失败
   * \param channel 新建立的管道
   */
  virtual void on_connect(const std::string & /*name*/, bool success,
                          kratos::service::BoxChannelPtr &channel) override;
  /**
   * 关闭管道事件
   *
   * \param channel 关闭的管道
   */
  virtual void on_close(kratos::service::BoxChannelPtr &channel) override;
  /**
   * 数据事件
   *
   * \param channel 接收到数据的管道
   */
  virtual void on_data(kratos::service::BoxChannelPtr &channel) override;

public:
#ifndef PYTHON_SDK
  /**
   * 初始化
   *
   * \param config_file_path 配置文件路径
   *
   * \return true成功, false失败
   */
  auto init(const char *config_file_path) -> bool;
  /**
   * 获取RpcProbeImpl
   */
  auto get_probe() -> kratos::service::RpcProbeImpl *;
#else
  /*
   * 初始化SDK模式
   *
   * \param config_file_path 配置文件路径
   *
   * \return true成功, false失败
   */
  auto init_sdk(const char *config_file_path) -> bool;

  /*
   * 连接代理网关
   *
   * \param hosts 网关地址列表
   *
   * \return true成功, false失败
   */
  auto connect_proxy(const std::vector<std::string> &hosts) -> bool;

  /*
   * 获取服务管道
   *
   * \param service_name 服务名
   *
   * \return 管道ID
   */
  auto get_channel(const std::string &service_name) -> std::uint64_t;

  /*
   * 关闭服务管道
   *
   */
  auto close_channel() -> void;

  /*
   * 重连检测
   */
  auto check_connection(std::time_t now) -> void;
#endif // PYTHON_SDK
  /**
   * 清理
   *
   * \param py_clean 是否允许内部清理Python相关资源
   * \return
   */
  auto deinit(bool py_clean = true) -> void;
  /**
   * 尝试获取一个协议对象
   *
   * \return 协议对象
   */
  auto pop() -> PyObject *;
  /**
   * 设置日志回调函数
   *
   * \param log_cb_obj
   * \return
   */
  auto set_log_cb(PyObject *log_cb_obj) -> void;
  /**
   * 序列化rpc::RpcCallRequestHeader及协议对象为字节流并发送
   *
   * \param serviceUUID 服务UUID
   * \param serviceID 服务实例ID
   * \param callID 调用ID
   * \param methodID 方法ID
   * \param obj_ptr 协议对象
   * \return true成功, false失败
   */
  auto call(std::uint64_t channel_id, rpc::ServiceUUID serviceUUID,
            rpc::ServiceID serviceID, rpc::CallID callID,
            rpc::MethodID methodID, PyObject *obj_ptr) -> bool;
  /**
   * 序列化rpc::RpcCallRetHeader及协议对象为字节流并发送
   *
   * \param channel_id 管道
   * \param serviceUUID 服务UUID
   * \param methodID 方法ID
   * \param serviceID 服务ID
   * \param callID 调用ID
   * \param errorID 错误ID
   * \param obj_ptr 协议对象
   * \return true成功, false失败
   */
  auto call_ret(std::uint64_t channel_id, rpc::ServiceUUID serviceUUID,
                rpc::MethodID methodID, rpc::ServiceID serviceID,
                rpc::CallID callID, rpc::ErrorID errorID, PyObject *obj_ptr)
      -> bool;
  /**
   * 序列化rpc::RpcProxyCallRequestHeader及协议对象为字节流并发送
   *
   * \param channel_id 管道
   * \param serviceUUID 服务UUID
   * \param methodID 方法ID
   * \param serviceID 服务ID
   * \param callID 调用ID
   * \param globalIndex 外部客户端标识
   * \param oneway 是否是单路方法
   * \param obj_ptr 协议对象
   * \return true成功, false失败
   */
  auto call_proxy(std::uint64_t channel_id, rpc::ServiceUUID serviceUUID,
                  rpc::ServiceID serviceID, rpc::CallID callID,
                  rpc::MethodID methodID, rpc::GlobalIndex globalIndex,
                  std::uint16_t oneway, PyObject *obj_ptr) -> bool;
  /**
   * 序列化rpc::RpcProxyCallRetHeader及协议对象为字节流并发送
   *
   * \param channel_id 管道
   * \param serviceUUID 服务UUID
   * \param methodID 方法ID
   * \param serviceID 服务ID
   * \param callID 调用ID
   * \param errorID 错误ID
   * \param globalIndex 外部客户端标识
   * \param obj_ptr 协议对象
   * \return true成功, false失败
   */
  auto call_proxy_ret(std::uint64_t channel_id, rpc::ServiceUUID serviceUUID,
                      rpc::MethodID methodID, rpc::ServiceID serviceID,
                      rpc::CallID callID, rpc::ErrorID errorID,
                      rpc::GlobalIndex globalIndex, PyObject *obj_ptr) -> bool;
  /**
   * 记录一次调用
   *
   * \param call_id 调用ID
   * \param uuid 服务UUID
   * \param method_id 方法ID
   * \param serviceID 服务实例ID
   * \param timeout 超时, 毫秒
   */
  auto reg_proxy_call(rpc::CallID call_id, rpc::ServiceUUID uuid,
                      rpc::MethodID method_id, rpc::ServiceID service_id,
                      std::time_t timeout) -> void;
  /**
   * 发起一次订阅
   *
   * @param channel_id 管道ID
   * @param sub_id 订阅ID
   * @param proxy_id 代理ID
   * @param serviceUUID 服务UUID
   * @param methodID 方法ID
   * @param serviceID 服务实例ID
   * @param evt_name 事件名
   * @param obj_ptr 附带的数据对象
   * @return true成功, false失败
   */
  auto subscribe(std::uint64_t channel_id, const std::string &sub_id,
                 rpc::ProxyID proxy_id, rpc::ServiceUUID serviceUUID,
                 rpc::MethodID methodID, rpc::ServiceID serviceID,
                 const std::string &evt_name, PyObject *obj_ptr) -> bool;
  /**
   * 发起一次订阅
   *
   * @param channel_id 管道ID
   * @param sub_id 订阅ID
   */
  auto cancel(std::uint64_t channel_id, const std::string &sub_id) -> void;
  /**
   * 发布
   *
   * @param channel_id 管道ID
   * @param sub_id 订阅ID
   * @param proxy_id 代理ID
   * @param obj_ptr 附带的数据对象
   */
  auto publish(std::uint64_t channel_id, const std::string &sub_id,
               rpc::ProxyID proxy_id, PyObject *obj_ptr) -> void;
  /**
   * 注册一个服务到集群
   *
   * \param service_name 服务名
   * \return true成功, false失败
   */
  auto register_service(const std::string &service_name) -> bool;
  /**
   * 反注册一个服务
   *
   * \param service_name 服务名
   * \return true成功, false失败
   */
  auto unregister_service(const std::string &service_name) -> bool;
  /**
   * 获取服务本地缓存
   *
   * \return 服务本地缓存
   */
  auto get_service_layer() -> kratos::service::ServiceLayer *;
  /**
   * 建立定时器
   *
   * \param timeout 超时, 毫秒
   * \param usr_data 用户数据
   */
  auto new_timer(std::time_t timeout, std::uint64_t usr_data) -> PyObject *;

public:
  /**
   * 写入日志, 会自动添加`[rpc]`前缀
   *
   * \param log_line 日志
   */
  auto write_log(const std::string &log_line) -> void;
  /**
   * 写入日志, 会自动添加`[rpc][erro]`前缀
   *
   * \param log_line 日志
   */
  auto write_error_log(const std::string &log_line) -> void;
  /**
   * 写入日志, 会自动添加`[rpc][warn]`前缀
   *
   * \param log_line 日志
   */
  auto write_info_log(const std::string &log_line) -> void;

private:
  auto on_call(std::uint64_t channel_id,
               rpc::RpcCallRequestHeader *call_req_ptr, const char *data,
               int length) -> PyObject *;
  auto on_call_ret(std::uint64_t channel_id,
                   rpc::RpcCallRetHeader *call_ret_ptr, const char *data,
                   int length) -> PyObject *;
  auto on_proxy_call(std::uint64_t channel_id,
                     rpc::RpcProxyCallRequestHeader *prx_call_req_ptr,
                     const char *data, int length) -> PyObject *;
  auto on_proxy_call_ret(std::uint64_t channel_id,
                         rpc::RpcProxyCallRetHeader *call_req_ptr,
                         const char *data, int length) -> PyObject *;
  auto on_sub(std::uint64_t channel_id, rpc::RpcSubHeader *sub_header_ptr,
              const char *data, int length) -> PyObject *;
  auto on_cancel(std::uint64_t channel_id,
                 rpc::RpcCancelSubHeader *cancel_header_ptr) -> PyObject *;
  auto on_pub(std::uint64_t channel_id, rpc::RpcPubHeader *pub_header_ptr,
              const char *data, int length) -> PyObject *;
  auto deserialize_arg(const char *data, int size, rpc::ServiceUUID uuid,
                       rpc::MethodID method_id) -> PyObject *;
  auto deserialize_ret(const char *data, int size, rpc::ServiceUUID uuid,
                       rpc::MethodID method_id) -> PyObject *;
  auto get_msg_arg(rpc::ServiceUUID, rpc::MethodID) -> ProtobufMessage *;
  auto get_msg_ret(rpc::ServiceUUID, rpc::MethodID) -> ProtobufMessage *;
#ifndef PYTHON_SDK
  auto on_get_trace_info(ProtobufMessage &msg, PyObject *obj_ptr) -> void;
#endif // !PYTHON_SDK

private:
  /**
   * 运行一帧主循环.
   *
   * \return
   */
  auto runtime_update() -> void;
  /**
   * 获取一个缓冲区指针.
   *
   * \param length 缓冲区长度
   * \return 缓冲区指针
   */
  auto get_buffer_ptr(std::size_t length) -> char *;
  /**
   * 记录调用信息.
   *
   * \param call_id 调用ID
   * \param uuid 服务UUID
   * \param method_id 方法ID
   * \param service_id 服务ID
   * \param timeout 超时, 毫秒
   * \return
   */
  auto set_call_info(rpc::CallID call_id, rpc::ServiceUUID uuid,
                     rpc::MethodID method_id, rpc::ServiceID service_id,
                     std::time_t timeout) -> void;
  /**
   * 获取调用信息
   *
   * \param call_id 调用ID
   * \return 调用信息
   */
  auto get_call_info(rpc::CallID call_id) -> const CallInfo &;
  /**
   * 建立超时事件对象.
   *
   * \param service_id 服务ID
   * \param call_id 调用ID
   * \return 超时事件对象
   */
  auto new_call_timeout_pack(rpc::ServiceID service_id, rpc::CallID call_id)
      -> PyObject *;
  /**
   * 建立非RPC超时事件对象.
   *
   * \param usr_data 用户数据
   * \return 超时事件对象
   */
  auto new_timeout_pack(std::uint64_t usr_data) -> PyObject *;
  /**
   * 超时定时器回调函数, 用于产生超时事件.
   *
   * \param usr_data 用户数据
   * \param ms 当前毫秒时间戳
   * \return true成功, false失败
   */
  auto timer_cb(std::uint64_t usr_data, std::time_t ms) -> bool;
  /**
   * 超时定时器回调函数, 用于产生非RPC超时事件.
   *
   * \param usr_data 用户数据
   * \param ms 当前毫秒时间戳
   * \return true成功, false失败
   */
  auto external_timer_cb(std::uint64_t usr_data, std::time_t ms) -> bool;
  /**
   * 随机选择一个监听器地址.
   *
   * \return 监听器地址
   */
  auto random_get_listener_host() -> std::string;
  /**
   * 反注册所有已经注册的服务名.
   *
   * \return
   */
  auto unregister_all_service() -> void;
  /**
   * 获取一个数据包对象
   *
   * \param channel 管道
   * \return 数据包对象
   */
  auto pop(kratos::service::BoxChannelPtr &channel) -> PyObject *;
  /**
   * 启动所有配置的监听器
   *
   * \return true成功, false失败
   */
  auto start_all_listener() -> bool;

  friend class ProtobufCoder;
};

} // namespace util
} // namespace kratos
