#pragma once

#include "readwritequeue/readerwriterqueue.h"

namespace kratos {
namespace corelib {

// Single producer, single consumer(SPSC) queue based on ring buffer
// under the environment of two thread
template <typename T, size_t MAX_BLOCK_SIZE = 1024 * 256>
class SPSCQueue : public moodycamel::ReaderWriterQueue<T, MAX_BLOCK_SIZE> {
public:
  explicit SPSCQueue(size_t maxSize = 64 * 1024)
      : moodycamel::ReaderWriterQueue<T, MAX_BLOCK_SIZE>(maxSize) {}
  ~SPSCQueue() {}

private:
  SPSCQueue(const SPSCQueue &) = delete;
  SPSCQueue(SPSCQueue &&) = delete;
  SPSCQueue &operator=(const SPSCQueue &) = delete;
};

} // namespace corelib
} // namespace kratos
