#pragma once

#include "util/bucket_token/token_producer.hh"
#include <cstdint>
#include <limits>

namespace kratos {
namespace util {

/**
 * 默认令牌产生器实现类.
 */
class TokenProducerImpl : public TokenProducer {
  std::size_t count_{0};           ///< 当前剩余令牌数
  std::size_t count_sec_{65535};   ///< 每秒产生的令牌数
  std::time_t update_interval_{1}; ///< 更新周期
  std::time_t last_ms_{0};         ///< 上一次更新的时间戳
  std::size_t bucket_volume_{0};   ///< 所属桶的容量

public:
  TokenProducerImpl();
  virtual ~TokenProducerImpl();
  virtual auto set_rate_per_second(std::size_t count) -> void override;
  virtual auto consume() -> bool override;
  virtual auto update(std::time_t now) -> std::size_t override;
  virtual auto token_count() -> std::size_t override;
  virtual auto set_volume(std::size_t bucket_volume) -> void override;
};

} // namespace util
} // namespace kratos
