#include "token_bucket_impl.hh"
#include "token_consumer_impl.hh"
#include "util/bucket_token/token_producer.hh"
#include "util/object_pool.hh"
#include <unordered_map>
#include "util/singleton.hh"

namespace kratos {
namespace util {

TokenBucketImpl::TokenBucketImpl(TokenConsumerPtr consumer,
                                 TokenProducerPtr producer) {
  consumer_ = consumer;
  producer_ = producer;
}
TokenBucketImpl::~TokenBucketImpl() {}
auto TokenBucketImpl::update(std::time_t now) -> void {
  producer_->update(now);
}
auto TokenBucketImpl::get_consumer() -> TokenConsumerPtr { return consumer_; }

auto TokenBucketImpl::set_volume(std::size_t volume) -> void {
  volume_ = volume;
  producer_->set_volume(volume);
}

auto TokenBucketImpl::consume() -> bool { return consumer_->consume(); }

using ProducerFactoryMap =
    std::unordered_map<std::string, TokenProducerFactoryPtr>;

ProducerFactoryMap global_producer_factory_map() {
  static ProducerFactoryMap global_producer_factory_map;
  return global_producer_factory_map;
}

auto register_token_producer_factory(const std::string &token_producer_type,
                                     TokenProducerFactoryPtr factory) -> bool {
  if (token_producer_type.empty()) {
    return false;
  }
  global_producer_factory_map()[token_producer_type] = factory;
  return true;
}
/**
 * ����һ������Ͱ.
 */
auto create_token_bucket(const std::string &token_producer_type)
    -> TokenBucketPtr {
  auto factory_it = global_producer_factory_map().find(token_producer_type);
  if (factory_it == global_producer_factory_map().end()) {
    return nullptr;
  }
  auto producer_ptr = factory_it->second->create_producer();
  if (!producer_ptr) {
    return nullptr;
  }
  auto consumer_ptr =
      kratos::make_shared_pool_ptr<TokenConsumerImpl>(producer_ptr);
  if (!consumer_ptr) {
    return nullptr;
  }
  return kratos::make_shared_pool_ptr<TokenBucketImpl>(consumer_ptr,
                                                       producer_ptr);
}

} // namespace util
} // namespace kratos
