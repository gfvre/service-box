#include "token_producer_impl.hh"
#include "util/bucket_token/token_bucket.hh"
#include "util/object_pool.hh"

namespace kratos {
namespace util {

TokenProducerImpl::TokenProducerImpl() {}

TokenProducerImpl::~TokenProducerImpl() {}

auto TokenProducerImpl::set_rate_per_second(std::size_t count) -> void {
  if (count == 0) {
    return;
  }
  count_sec_ = count;
  if (update_interval_ > 1000) {
    // 帧率不小于1
    update_interval_ = 1000;
  }
  // 生成一个update周期的令牌
  count_ = std::size_t(count_sec_ *
                       std::size_t((float(update_interval_) / 1000.0f)));
  if (bucket_volume_ == 0) {
    bucket_volume_ = count_sec_;
  } else {
    // 可用令牌数不超过桶容量
    if (bucket_volume_ < count_) {
      count_ = bucket_volume_;
    }
  }
}

auto TokenProducerImpl::consume() -> bool {
  if (count_ == 0) {
    return false;
  }
  count_ -= 1;
  return true;
}

auto TokenProducerImpl::update(std::time_t now) -> std::size_t {
  if (last_ms_ == 0) {
    // 第一帧记录时间
    last_ms_ = now;
    return 0;
  }
  // 未设定桶容量, 桶容量为每秒最大令牌数
  if (bucket_volume_ == 0) {
    bucket_volume_ = count_sec_;
  }
  auto last_count = count_;
  // 调整更新周期
  update_interval_ = std::size_t(now - last_ms_);
  last_ms_ = now;
  // 计算一帧产生的令牌数
  auto gen_count = std::size_t(
      count_sec_ * std::size_t((float(update_interval_) / 1000.0f)));
  // 增加令牌
  if (count_ + gen_count < std::numeric_limits<std::size_t>::max()) {
    count_ += gen_count;
  } else {
    // 令牌达到数值上限
    gen_count = (std::numeric_limits<std::size_t>::max() - count_);
    count_ = std::numeric_limits<std::size_t>::max();
  }
  // 可用令牌数不超过桶容量
  if (bucket_volume_ < count_) {
    count_ = bucket_volume_;
  }
  return (count_ > last_count ? count_ - last_count : 0);
}

auto TokenProducerImpl::token_count() -> std::size_t { return count_; }

auto TokenProducerImpl::set_volume(std::size_t bucket_volume) -> void {
  bucket_volume_ = bucket_volume;
}

/**
 * 令牌产生器实现类.
 */
class TokenProducerFactoryImpl : public TokenProducerFactory {
public:
  TokenProducerFactoryImpl() {}
  virtual ~TokenProducerFactoryImpl() {}
  virtual auto create_producer() -> TokenProducerPtr override {
    return kratos::make_shared_pool_ptr<TokenProducerImpl>();
  }
};

// 注册默认令牌产生器工厂
auto _reg_token_producer_res_ = register_token_producer_factory(
    "default", kratos::make_shared_pool_ptr<TokenProducerFactoryImpl>());

} // namespace util
} // namespace kratos
