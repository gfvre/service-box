#pragma once

#include <ctime>
#include <string>

#include "bucket_token_defines.hh"

namespace kratos {
namespace util {

class TokenProducer;
class TokenConsumer;

/**
 * 令牌桶.
 */
class TokenBucket {
public:
  virtual ~TokenBucket() {}
  /**
   * 主循环.
   *
   * \param now 当前时间戳，毫秒
   * \return
   */
  virtual auto update(std::time_t now) -> void = 0;
  /**
   * 获取令牌消费者.
   *
   * \return
   */
  virtual auto get_consumer() -> TokenConsumerPtr = 0;
  /**
   * 设置令牌桶容量.
   *
   * \param volume
   * \return
   */
  virtual auto set_volume(std::size_t volume) -> void = 0;
  /**
   * 是否限流.
   *
   * \return true是，false否
   */
  virtual auto consume() -> bool = 0;
};
/**
 * 注册一个令牌消费者工厂.
 */
extern auto
register_token_producer_factory(const std::string &token_producer_type,
                                TokenProducerFactoryPtr factory) -> bool;
/**
 * 创建一个令牌桶.
 */
extern auto create_token_bucket(const std::string &token_producer_type)
    -> TokenBucketPtr;

} // namespace util
} // namespace kratos
