#pragma once

#include "call_tracer/call_tracer.hh"
#include "call_tracer/call_tracer_factory.hh"
#include "root/rpc_probe.h"
#include <cstdint>
#include <random>
#include <unordered_map>

namespace kratos {
namespace config {
class BoxConfig;
}
} // namespace kratos

namespace kratos {
namespace service {

class ServiceBox;

/**
 * RPC探针实现.
 */
class RpcProbeImpl : public rpc::RpcProbe {
  ServiceBox *box_{nullptr};           ///< ServiceBox实例指针
  config::BoxConfig *config_{nullptr}; ///< 配置指针
  std::int32_t pid_{0};                ///< 当前进程PID
  CallTracerPtr call_tracer_ptr_;      ///< 调用链跟踪实例
  std::string mac_addr_;               ///< MAC地址
  std::string mac_hash_str_;           ///< MAC地址哈希字符串
  std::uint32_t ip_hash_{0};           ///< IP地址哈希
  std::string pid_str_;                ///< PID字符串
  std::uint64_t trace_id_{1};          ///< trace自增ID
  std::uint16_t id_{1};                ///< span自增ID
  std::uint16_t port_{0};              ///< 监听端口，用来产生span ID

  using TraceMap = std::unordered_map<std::uint64_t, TraceInfo>;
  TraceMap proxy_call_trace_map_; ///< Proxy call trace map
  TraceMap stub_call_trace_map_; ///< Stub call trace map

public:
  RpcProbeImpl(ServiceBox *box);
  RpcProbeImpl(config::BoxConfig *config);
  virtual ~RpcProbeImpl();
  /**
   * @brief probe message
   * @param msg_type message type
   * @param transport transport
   * @return
   */
  virtual auto on_msg(rpc::RpcMsgType msg_type, rpc::TransportPtr &transport)
      -> void override;
  /**
   * @brief Invoked by RPC framework each tick
   * @param now Current timestamp in millionseconds
   * @return
   */
  virtual auto on_tick(std::time_t now) -> void override;
  /**
   * @brief Invoked by RPC framework when encounter error
   * @param error_id Error ID
   * @param error_msg error message
   * @return
   */
  virtual auto on_error(std::uint32_t error_id, const std::string &error_msg)
      -> void override;
  /**
   * @brief Invoked by RPC framework when framework shutdown
   * @return
   */
  virtual auto on_close() -> void override;
  /**
   * @brief Invoked by RPC framework when finish to load bundle
   * @param uuid The UUID of service
   * @param bundle_path The path of bundle
   * @param result Operation result
   * @return
   */
  virtual auto on_load(const std::string &uuid, const std::string &bundle_path,
                       bool result) -> void override;
  /**
   * @brief Invoked by RPC framework when finished to unload bundle
   * @param uuid The UUID of service
   * @param result Operation result
   * @return
   */
  virtual auto on_unload(const std::string &uuid, bool result) -> void override;

  virtual auto on_gen_uuid(std::string &uuid_str) -> void override;
  virtual auto on_gen_uuid(std::uint64_t &uuid) -> void override;
  virtual auto on_get_micro_now(std::time_t &micro_now) -> void override;
  virtual auto
  on_proxy_call_sent(const std::string &trace_id, std::uint64_t span_id,
                     std::uint64_t parent_span_id, rpc::TransportPtr transport,
                     const std::string &target_service_name,
                     const std::string &target_method_name,
                     const std::string &args_info, bool oneway = false)
      -> void override;
  virtual auto on_stub_call_arrived(const std::string &trace_id,
                                    std::uint64_t span_id,
                                    std::uint64_t parent_span_id,
                                    rpc::TransportPtr transport,
                                    const std::string &target_service_name,
                                    const std::string &target_method_name,
                                    const std::string &args_info)
      -> void override;
  virtual auto on_stub_call_return_sent(const std::string &trace_id,
                                        std::uint64_t span_id,
                                        std::uint64_t parent_span_id,
                                        rpc::TransportPtr transport,
                                        const std::string &target_service_name,
                                        const std::string &target_method_name,
                                        const std::string &retval_info)
      -> void override;
  virtual auto on_proxy_call_return_arrived(
      const std::string &trace_id, std::uint64_t span_id,
      std::uint64_t parent_span_id, rpc::TransportPtr transport,
      const std::string &target_service_name,
      const std::string &target_method_name, const std::string &retval_info,
      std::time_t start_timestamp = 0) -> void override;

private:
  /**
   * 获取第一个容器监听的端口.
   *
   * \return
   */
  auto get_listener_port() -> std::uint16_t;
};

} // namespace service
} // namespace kratos
