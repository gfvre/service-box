#include "call_tracer_kafka.hh"

namespace kratos {
namespace service {

CallTracerKafka::CallTracerKafka() {}

CallTracerKafka::~CallTracerKafka() {}

auto CallTracerKafka::start(kratos::config::BoxConfig &config) -> bool {
  return true;
}

auto CallTracerKafka::trace(const TraceInfo &trace_info) -> void {}

auto CallTracerKafka::update(std::time_t /*now*/) -> void {}

} // namespace service
} // namespace kratos
