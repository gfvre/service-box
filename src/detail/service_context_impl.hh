﻿#pragma once

#include "box/service_context.hh"
#include "detail/service_layer.hh"
#include <string>
#include <unordered_map>

namespace kratos {
namespace service {

class ServiceBox;

/**
 * 容器上下文实现类.
 */
class ServiceContextImpl : public ServiceContext {
  ServiceBox *box_{nullptr}; ///< 容器指针
  std::uint32_t cb_id_{1};   ///< 最大的Proxy回调ID
  // Proxy回调信息
  struct CbInfo {
    std::string service_name;      ///< 服务名
    TransportCallback cb;          ///< Proxy回调
    ServiceEventCallback trans_cb; ///< Tranport回调
  };
  using CbMap = PoolUnorederedMap<std::uint32_t, CbInfo>;
  CbMap cb_map_; ///< Proxy回调表

public:
  /**
   * 构造.
   *
   * \param box 容器指针
   */
  ServiceContextImpl(ServiceBox *box);
  /**
   * 析构.
   *
   */
  virtual ~ServiceContextImpl();
  virtual auto register_service(const std::string &name) -> bool override;
  virtual auto unregister_service(const std::string &name) -> bool override;
  virtual auto shutdown() -> void override;
  virtual void sleep_co(std::time_t ms) override;
  virtual auto get_transport(const std::string &service_name,
                             TransportCallback cb)
      -> CallbackAutoDeleterPtr override;
  virtual auto get_argument() const -> const argument::BoxArgument & override;
  virtual auto write_log_line(int log_level, const std::string &log_line)
      -> void override;
  virtual auto get_config() const -> kratos::config::BoxConfig & override;
  virtual auto try_get_transport(const std::string &name)
      -> std::shared_ptr<rpc::Transport> override;
  virtual auto get_transport_sync(const std::string &name, std::time_t timeout)
      -> std::shared_ptr<rpc::Transport> override;
  virtual auto get_transport_co(const std::string &service_name,
                                std::time_t timeout)
      -> std::shared_ptr<rpc::Transport> override;
  virtual auto verbose(const std::string &log) -> void override;
  virtual auto info(const std::string &log) -> void override;
  virtual auto diagnose(const std::string &log) -> void override;
  virtual auto warn(const std::string &log) -> void override;
  virtual auto except(const std::string &log) -> void override;
  virtual auto fail(const std::string &log) -> void override;
  virtual auto fatal(const std::string &log) -> void override;
  virtual auto new_scheduler() -> std::unique_ptr<Scheduler> override;
  virtual auto new_http() -> std::unique_ptr<kratos::http::HttpBase> override;
  virtual auto new_coro_runner() -> std::unique_ptr<CoroRunner> override;
  virtual auto get_allocator() -> MemoryAllocator & override;
  virtual auto new_command() -> std::unique_ptr<Command> override;
  virtual auto new_redis() -> std::unique_ptr<kratos::redis::Redis> override;
  virtual auto allocate(std::size_t size) -> void * override;
  virtual auto deallocate(void *p) -> void override;
  virtual auto get_statistics() -> ProcStat * override;
  virtual auto get_module_logger(const std::string &name)
      -> std::unique_ptr<ServiceLogger> override;
  virtual auto get_local_time()
      -> std::unique_ptr<kratos::time::LocalTime> override;
  virtual auto get_proxy_handler() -> rpc::ProxyHandler * override;
  virtual auto get_rpc() -> rpc::Rpc * override;
  virtual auto get_rpc_statistics() -> rpc::StubCallStatistics * override;
  virtual auto new_csv_manager() -> CsvManagerPtr override;
  virtual auto new_box_console(const std::string &name)
      -> BoxConsolePtr override;
  virtual auto new_lua_service() -> LuaServicePtr override;
  virtual auto new_util() -> UtilPtr override;
  virtual auto get_remote_proxy_transport()
      -> std::shared_ptr<rpc::Transport> override;
  virtual auto get_component(const std::string &comp_name,
                             const std::string &version = "")
      -> component::ComponentPtr override;
  virtual auto add_proxy_cb(const std::string &service_name,
                            TransportCallback cb) -> std::uint32_t override;
  virtual auto remove_proxy_cb(std::uint32_t cb_id) -> void override;

public:
  /**
   * 获取服务容器.
   *
   * \return 服务容器
   */
  auto get_box() -> ServiceBox *;

  friend class TransportWaiterImpl;
};

class CallbackAutoDeleterImpl : public CallbackAutoDeleter {
  ServiceContextImpl *ctx_ptr_{nullptr};
  std::uint32_t cb_id_{0};

public:
  CallbackAutoDeleterImpl(ServiceContextImpl *ctx_ptr, std::uint32_t cb_id);
  virtual ~CallbackAutoDeleterImpl();
  virtual auto get_cb_id() -> std::uint32_t override;
};

} // namespace service
} // namespace kratos
