#include "box_alloc.hh"

#include "box/fixed_mem_pool.hh"
#include <cstdlib>

char *kratos::service::box_malloc(std::size_t size) {
#ifndef DISABLE_SB_CODE
  return reinterpret_cast<char *>(MempoolRef.rent(size));
#else
  return reinterpret_cast<char *>(malloc(size));
#endif
}

void kratos::service::box_free(void *p) {
  if (!p) {
    return;
  }
#ifndef DISABLE_SB_CODE
  MempoolRef.recycle(reinterpret_cast<char *>(p));
#else
  return free(p);
#endif
}
