#pragma once

#include <functional>
#include <memory>
#include <string>
#include <typeinfo>

class BoxSDK;

using SDKEventFunction = std::function<void(BoxSDK &)>;

namespace rpc {
class Proxy;
}

/**
 * SDK框架接口.
 */
class BoxSDK {
public:
  /**
   * 析构函数.
   *
   */
  virtual ~BoxSDK() {}
  /**
   * 初始化，在使用任何其他函数前必须调用这个函数进行初始化，只需要调用一次.
   *
   * \param config_file 配置文件路径
   * \param [IN OUT] error_str 如果发生错误，这个参数将会被填充
   * \param user_ptr 用户指针, 用户实现的service内调用getContext()获取
   * \return true成功, false失败
   */
  virtual auto initialize(const std::string &config_file,
                          std::string &error_str,
                          void *user_ptr = nullptr) noexcept(true) -> bool = 0;
  /**
   * 清理并释放框架资源.
   *
   * \return
   */
  virtual auto deinitialize() -> void = 0;
  /**
   * SDK框架主循环，每帧调用一次.
   *
   * \return
   */
  virtual auto tick() -> void = 0;
  /**
   * 检测SDK框架是否与远程服务器连接.
   *
   * \return true连接正常，false连接已断开或未连接成功
   */
  virtual auto is_connected() -> bool = 0;
  /**
   * 连接到远程服务器.
   *
   * \param evt_func 回调函数
   */
  virtual void on_connected(SDKEventFunction evt_func) = 0;
  /**
   * 与远程服务器断开连接.
   *
   * \param evt_func 回调函数
   */
  virtual void on_disconnected(SDKEventFunction evt_func) = 0;
  /**
   * 写日志
   * \param log 日志
   */
  virtual void write_log(const std::string &log) = 0;
  /**
   * 获取用户指针, 调用initialize时提供
   * \return 用户指针
   */
  virtual auto get_usr_ptr() -> void* = 0;
  /**
   * 获取指定类型T的代理.
   *
   * \param error_str [IN OUT] error_str 如果发生错误，这个参数将会被填充
   * \return 类型未T*的代理指针
   */
  template <typename T> inline auto get_proxy(std::string &error_str) -> T *;

protected:
  /**
   * 根据类型名获取代理指针.
   *
   * \param ref_str 类型名
   * \param [IN OUT] error_str error_str 如果发生错误，这个参数将会被填充
   * \return 类型未void*的代理指针
   */
  virtual auto get_proxy_raw(const std::string &ref_str, std::string &error_str)
      -> rpc::Proxy * = 0;
  /**
   * 获取类名的后缀，通常在内部实现类型T对应的名字实际为T+后缀，不同实现通过此方法返回实际实现的后缀.
   *
   * \return 类名的后缀
   */
  virtual auto get_class_name_suffix() -> const std::string & = 0;
  /**
   * 获取类名的前缀，通常在内部实现类型T对应的名字实际为前缀+T，不同实现通过此方法返回实际实现的前缀.
   *
   * \return
   */
  virtual auto get_class_name_prefix() -> const std::string & = 0;
};

template <typename T>
inline auto BoxSDK::get_proxy(std::string &error_str) -> T * {
  // 获取并强制转换为指定类型
  return dynamic_cast<T *>(get_proxy_raw(std::to_string(T::uuid()), error_str));
}
