# C++ proxy异步调用

服务代理(service proxy)支持异步调用方式与远程service实例进行RPC通信。

假设有如下服务:

```
service dynamic Login multiple=8 {
    ui64 login(string, string)
    void logout(ui64)
}
```

框架生成工具会生成如下的proxy头文件:

```
// Machine generated code

#pragma once

#include <string>
#include <cstdint>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <memory>
#include <functional>
#include "rpc_proxy.h"
#include "example.struct.h"

using namespace example_cpp;

// 下面两个方法为RPC异步回调
using Login_login_callback = std::function<void(std::uint64_t,rpc::RpcError)>;
using Login_logout_callback = std::function<void(rpc::RpcError)>;

class LoginProxy : public virtual rpc::Proxy {
public:
    virtual ~LoginProxy() {}
    static std::uint64_t uuid() { return std::uint64_t(5871407834537456905); }

    ......

    // 下面两个方法为RPC异步方法原型
    // 1. 异步方法都是没有返回值的
    // 2. 返回值将通过回调函数参数获取
    virtual void login(const std::string&,const std::string&,Login_login_callback) = 0;
    virtual void logout(std::uint64_t,Login_logout_callback) = 0;
};

using LoginPrx = std::shared_ptr<LoginProxy>;
```

调用方服务实现内, 假设注册的服务路径为'/service/login', 调用Login服务代理的异步版本:
```
// 在某处获取服务代理，可以保存供后续使用
auto login_prx = getContext()->try_get_proxy("/service/login");
// 调用服务代理方法
if (login_prx) {
  login_prx->login("user", "pass", [&](std::uint64_t id, rpc::RpcError error) {
    if (error == rpc::RpcError::NOT_FOUND) {
      // 方法不存在
    } else if (error == rpc::RpcError::EXCEPTION) {
      // 对端服务调用过程发生C++异常
    } else if (error == rpc::RpcError::TIMEOUT) {
      // 调用超时
    } else {
      // 调用成功，返回值为: id
    }
  });
}
```