/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstring>
#include <ctime>

#include "klogger/interface/logger.h"
#include "mode.h"
#include "util.h"

using namespace klogger;

Mode::Mode() {
  _mode = DAY;
  _deadline = calcDeadline();
}

Mode::Mode(const std::string &mode, std::string fileName)
    : _fileName(fileName) {
  _mode = 0;

  if (mode == "hour") {
    _mode = HOUR;
  } else if (mode == "day") {
    _mode = DAY;
  } else if (mode == "week") {
    _mode = WEEK;
  } else if (mode == "single") {
    _mode = SINGLE;
  } else {
    throw LoggerException("invalid logger mode");
  }

  _deadline = calcDeadline();
}

Mode::~Mode() { _mode = 0; }

void Mode::parse(const std::string &mode, std::string fileName) {
  _fileName.parse(fileName);
  _mode = 0;

  if (mode == "hour") {
    _mode = HOUR;
  } else if (mode == "day") {
    _mode = DAY;
  } else if (mode == "week") {
    _mode = WEEK;
  } else {
    _mode = SINGLE;
  }

  _deadline = calcDeadline();
}

time_t Mode::calcDeadline() {
  if (_mode == SINGLE) {
    return 0;
  }

  time_t deadline = time(0);
  timeval tp;
  memset(&tp, 0, sizeof(tp));
  gettimeofday(&tp, 0);

  if (HOUR == _mode) {
    deadline = deadline + HOUR_TIME - tp.tv_sec % HOUR_TIME;
  } else if (DAY == _mode) {
    deadline = deadline + DAY_TIME - tp.tv_sec % DAY_TIME;
  } else if (WEEK == _mode) {
    deadline = deadline + WEEK_TIME - tp.tv_sec % WEEK_TIME;
  }

  return deadline;
}

bool Mode::reach() {
  if (0 == _deadline) {
    return false;
  }

  if (_deadline > time(0)) {
    return false;
  }

  _deadline = calcDeadline();
  return true;
}

std::string Mode::getFileName() { return _fileName.getPattern().getPrefix(); }
