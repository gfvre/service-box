﻿#include "load_balancer/load_balance.h"

namespace kratos {
namespace loadbalance {

struct SWRRSNode {
  SWRRSNode() = default;
  SWRRSNode(LoadBalanceNodeWeakPtr n) : lb_node(n), cur_weight(0) {
    effect_weight = lb_node.lock()->get_weight();
  }
  LoadBalanceNodeWeakPtr lb_node;
  std::int32_t cur_weight{0};
  std::int32_t effect_weight{0};
};

class SWRRSBalancer : public ILoadBalancer {
public:
  SWRRSBalancer() = default;
  virtual ~SWRRSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override{};

  static auto get_name() -> const std::string {
    return "SMOOTH_ROUND_ROBIN_BALANCER";
  }
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Smooth_Weighted_Round_Robin_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<SWRRSBalancer>();
  }

private:
  static bool smooth_weight_round_robin_register_;
  std::int32_t last_select_index_{-1};
  std::int32_t server_count_{0};
  std::string name_{""};
  std::vector<SWRRSNode> nodes_;
};

auto SWRRSBalancer::add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool {
  if (lbnode.expired()) {
    return false;
  }
  server_count_++;
  nodes_.emplace_back(SWRRSNode(lbnode));
  return true;
}

auto SWRRSBalancer::get_next(const std::string &/*ip*/) -> LoadBalanceNodeWeakPtr {
  if (server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  std::int32_t total_weight = 0;
  for (std::int32_t i = 0; i < server_count_; i++) {
    auto &node = nodes_[i];
    if (node.lb_node.expired()) {
      continue;
    }
    auto weight = node.lb_node.lock()->get_weight();
    if (weight == 0) {
      continue;
    }
    node.cur_weight += node.effect_weight;
    total_weight += node.effect_weight;
    if (node.effect_weight < weight) {
      node.effect_weight++;
    }
    if (last_select_index_ == -1 ||
        node.cur_weight > nodes_[last_select_index_].cur_weight) {
      last_select_index_ = i;
    }
  }
  if (last_select_index_ == -1) {
    return LoadBalanceNodeWeakPtr();
  }
  nodes_[last_select_index_].cur_weight -= total_weight;
  return nodes_[last_select_index_].lb_node;
}

auto SWRRSBalancer::clear() -> void {
  nodes_.clear();
  server_count_ = 0;
  last_select_index_ = -1;
}

Registe_Balancer(SWRRSBalancer, smooth_weight_round_robin_,
                 BalancerMod::Smooth_Weighted_Round_Robin_Scheduling,
                 SWRRSBalancer::creator)
} // namespace loadbalance
} // namespace kratos
