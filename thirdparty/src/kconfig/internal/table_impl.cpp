/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>

#include "table_impl.h"
#include "table_pair_impl.h"

TableImpl::TableImpl() {
  _current = _table.end();
  _fired = false;
}

TableImpl::~TableImpl() { _table.clear(); }

int TableImpl::getSize() { return (int)_table.size(); }

TablePair *TableImpl::get(const std::string &key) {
  auto iterator = _table.find(key);

  if (iterator == _table.end()) {
    return 0;
  }

  return iterator->second.get();
}

AttributePtr TableImpl::get_ptr(const std::string &key) {
  auto iterator = _table.find(key);

  if (iterator == _table.end()) {
    return nullptr;
  }

  return iterator->second->value_ptr();
}

bool TableImpl::hasNext() {
  if (_table.empty()) {
    return false;
  }

  if (!_fired) {
    _current = _table.begin();
    _fired = true;
  } else {
    if (_current == _table.end()) {
      _current = _table.begin();
      return false;
    }
  }

  return true;
}

TablePair *TableImpl::next() {
  if (_current == _table.end()) {
    return 0;
  }

  TablePairImpl *pair = _current->second.get();
  ++_current;
  return pair;
}

const std::string &TableImpl::name() {
  static std::string NullString;
  return NullString;
}

AttributePtr TableImpl::doOperate(int op, AttributePtr rhs) { return nullptr; }

auto TableImpl::to_string() -> std::string {
  std::string ret_str;
  for (const auto &[_, pair] : _table) {
    ret_str += "  " + pair->key() + ":" + pair->value()->to_string() + "\n";
  }
  return ret_str;
}

void TableImpl::add(const std::string &name, AttributePtr attribute) {
  auto iterator = _table.find(name);

  if (iterator != _table.end()) {
    throw std::runtime_error("duplicate name");
  }

  _table.insert(
      std::make_pair(name, std::make_shared<TablePairImpl>(name, attribute)));
}
